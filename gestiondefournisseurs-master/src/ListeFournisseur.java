public class ListeFournisseur {
    Fournisseur f = null;
    ListeFournisseur suivant = null;

    public static ListeFournisseur pop(Fournisseur nf, ListeFournisseur l) {
        if (l.f == null) {
            l.f = nf;
        } else {
            ListeFournisseur nl = new ListeFournisseur();
            ListeFournisseur tempL = getLast(l);
            nl.f = nf;
            tempL.suivant = nl;
        }
        return l;
    }

    public static Fournisseur findById (int id, ListeFournisseur l) {
        if (l.suivant == null && l.f.id == id) {
            return l.f;
        } else {
            while (l.suivant != null && l.f.id != id) {
                l = l.suivant;
            }

            if (l.suivant == null && l.f.id != id) {
                return null;
            } else {
                return l.f;
            }
        }
    }

    public static Fournisseur findByName (String nom, ListeFournisseur l) {
        if (l.suivant == null && l.f.nom.equals(nom)) {
            return l.f;
        } else {
            while (l.suivant != null && !l.f.nom.equals(nom)) {
                l = l.suivant;
            }

            if (l.suivant == null && !l.f.nom.equals(nom)) {
                return null;
            } else {
                return l.f;
            }
        }
    }

    private static ListeFournisseur getLast (ListeFournisseur l) {
        if (l.suivant != null) {
            while (l.suivant != null) {
                l = l.suivant;
            }
        }
        return l;
    }

    public static ListeFournisseur removeById (int id, ListeFournisseur l) {
        ListeFournisseur tempL = l;
        ListeFournisseur derniere = l;

        if (l.suivant == null && l.f.id == id) {
            l.f = null;
        } else if (l.suivant != null && l.f.id == id) {
            l = l.suivant;
        } else {
            while (tempL.suivant != null && tempL.f.id != id) {
                derniere = tempL;
                tempL = tempL.suivant;
            }

            if (tempL.suivant != null && tempL.f.id == id) {
                derniere.suivant = tempL.suivant;
            } else if (tempL.suivant == null && tempL.f.id == id) {
                derniere.suivant = null;
            }
        }
        return l;
    }
}

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    private static Scanner scan = new Scanner(System.in);
    private static int UID = 1;
    private static int cptLf = 0;
    private  static ListeFournisseur lf = new ListeFournisseur();

    public static void main(String[] args) {
        mainMenu();
    }

    private static void mainMenu() {
        afficherMenuDansMain(false);

        String in;

        do {
            in = scan.next();
            switch (in) {
                case "1":
                    ajouterFourni();
                    break;
                case "2":
                    listerFourni();
                    break;
                case "3":
                    rechercherParId();
                    break;
                case "4":
                    rechercherParNom();
                    break;
                case "5":
                    modifierFourni();
                    break;
                case "6":
                    suprimerFourni();
                    break;
                case "7":
                    System.exit(0);
                    break;
            }
        } while (!entreEstMainMenu(in));

    }

    private static void ajouterFourni() {
        String nom;
        String noPhone;
        String mailAddress;
        boolean etatCommande;

        if (cptLf > 50) {
            System.out.println("Le nombre des fournisseurs enregistrés a atteint maximum (50). Veuillez en supprimer pour continuer.");
            mainMenu();
            return;
        } else {
            System.out.println("Entrez le nom de fournisseur:\n*Le nom sans les espaces contient au moins un caractère");
            nom = scan.next();
            while (!nom.matches("^[\\S]+$")) {
                System.out.println("*Le nom sans les espaces contient au moins un caractère");
                nom = scan.next();
            }
            System.out.println("Entrez le numéro de téléphone de fournisseur:");
            noPhone = scan.next();
            System.out.println("Entrez l'adresse de mail de fournisseur:");
            mailAddress = scan.next();
            System.out.println("Entrez l'état de commande de fournisseur:");
            etatCommande = scan.nextBoolean();

            // validation de l'utilisateur

            afficherInfoAenregistrer(nom,noPhone, mailAddress, etatCommande, false);
            String in = scan.next();

             do {
                if (entreEstOui(in)) {
                    Fournisseur nouveauF = new Fournisseur();
                    nouveauF.id = UID;
                    nouveauF.nom = nom;
                    nouveauF.noPhone = noPhone;
                    nouveauF.mailAddress = mailAddress;
                    nouveauF.estCommande = etatCommande;

                    lf = ListeFournisseur.pop(nouveauF, lf);

                    UID++;
                    cptLf++;

                    System.out.println(">> Enregistrement effectué avec succès <<");
                    mainMenu();
                } else if (entreEstNon(in)) {
                    System.out.println(">> Retouner au menu principal <<");
                    mainMenu();
                } else {
                    afficherInfoAenregistrer(nom,noPhone, mailAddress, etatCommande, true);
                    in = scan.next();
                }
            } while (!entreEstOui(in) || !entreEstNon(in));
        }
    }

    private static boolean entreEstMainMenu (String  entre) {
        String[] confirmationDic = {"1", "2", "3", "4", "5", "6", "7"};
        boolean check = Arrays.asList(confirmationDic).contains(entre);
        if (check) {
            return true;
        } else {
            afficherMenuDansMain(true);
            return false;
        }
    }

    private static boolean entreEstOui(String entre) {
        String[] confirmationDic = {"oui", "Oui", "OUI", "o", "O"};
        return Arrays.asList(confirmationDic).contains(entre);
    }

    private static boolean entreEstNon(String entre) {
        String[] confirmationDic = {"non", "Non", "NON", "n", "N",};
        return Arrays.asList(confirmationDic).contains(entre);
    }

    private static void listerFourni() {
        if (cptLf == 0) {
            System.out.println(">> 0 enregistrement trouvé <<\n");
            mainMenu();
        }

        ListeFournisseur tempL = lf;
        Fournisseur currF;

        while (tempL.suivant != null) {
            currF = tempL.f;
            afficherInfoIndividuelle(currF);
            tempL = tempL.suivant;
        }

        currF = tempL.f;
        afficherInfoIndividuelle(currF);
        System.out.printf(">> %d enregistrement(s) trouvé(s) <<\n\n", cptLf);

        mainMenu();
    }

    private static void rechercherParId() {
        if (cptLf == 0) {
            System.out.println(">> 0 enregistrement à rechercher <<\n");
            mainMenu();
        }

        System.out.println("Entrez le numéro de fournisseur pour la recherche: ");
        String entreId = scan.next();
        while (pasNumerique(entreId)) {
            System.out.println("Entrez le numéro de fournisseur: ");
            entreId = scan.next();
        }
        Fournisseur resultat = ListeFournisseur.findById(Integer.valueOf(entreId), lf);
        afficherInfoIndividuelle(resultat);

        mainMenu();
    }

    private static boolean pasNumerique(String str) {
        return str == null || !str.matches("[0-9.]+");
    }

    private static void rechercherParNom() {
        if (cptLf == 0) {
            System.out.println(">> 0 enregistrement à rechercher <<\n");
            mainMenu();
        }

        System.out.println("Entrez le nom de fournisseur pour la recherche: ");
        String entreNom = scan.next();

        Fournisseur resultat = ListeFournisseur.findByName(entreNom, lf);
        afficherInfoIndividuelle(resultat);

        mainMenu();
    }

    private static void modifierFourni() {
        if (cptLf == 0) {
            System.out.println(">> 0 enregistrement à supprimer <<\n");
            mainMenu();
        }

        String entre = afficherEntreModification();

        String nouveauNom;
        String nouveauNoPhone;
        String nouveauAdMail;
        boolean nouvelEtat;

        while (pasNumerique(entre) && !entre.equals("aide")) {
            entre = afficherEntreModification();
        }

        Fournisseur resultat;

        if (entre.equals("aide")) {
            rechercherParNom();
        } else if (!pasNumerique(entre)) {
            resultat = ListeFournisseur.findById(Integer.valueOf(entre), lf);

            nouveauNom = resultat.nom;
            nouveauNoPhone = resultat.noPhone;
            nouveauAdMail = resultat.mailAddress;
            nouvelEtat = resultat.estCommande;

            afficherInfoIndividuelle(resultat);

            afficherMenuDansModi();

            String in;

            do {
                in = scan.next();
                switch (in) {
                    case "1":
                        System.out.println("Saisissez un nouveau nom :");
                        nouveauNom = scan.next();
                        while (!nouveauNom.matches("^[\\S]+$")) {
                            System.out.println("*Le nom sans les espaces contient au moins un caractère");
                            nouveauNom = scan.next();
                        }
                        afficherInfoAenregistrer(nouveauNom,nouveauNoPhone, nouveauAdMail, nouvelEtat, false);
                        break;
                    case "2":
                        System.out.println("Saisissez un nouveau numéro de téléphone :");
                        nouveauNoPhone = scan.next();
                        afficherInfoAenregistrer(nouveauNom,nouveauNoPhone, nouveauAdMail, nouvelEtat, false);
                        break;
                    case "3":
                        System.out.println("Saisissez un nouvel adresse de mail :");
                        nouveauAdMail = scan.next();
                        afficherInfoAenregistrer(nouveauNom,nouveauNoPhone, nouveauAdMail, nouvelEtat, false);
                        break;
                    case "4":
                        System.out.println("Saisissez l'état de commande :");
                        nouvelEtat = scan.nextBoolean();
                        afficherInfoAenregistrer(nouveauNom,nouveauNoPhone, nouveauAdMail, nouvelEtat, false);
                        break;
                }
            } while (!entreEstModiMenu(in));

            in = scan.next();

            do {
                if (entreEstOui(in)) {
                    resultat.nom = nouveauNom;
                    resultat.noPhone = nouveauNoPhone;
                    resultat.mailAddress = nouveauAdMail;
                    resultat.estCommande = nouvelEtat;
                    System.out.println(">> Modification effectuée avec succès <<");
                    mainMenu();
                } else if (entreEstNon(in)) {
                    System.out.println(">> Retouner au menu principal <<");
                    mainMenu();
                } else {
                    System.out.println("Veuillez saisir oui [o] ou non [n] pour confirmer la modification: ");
                    in = scan.next();
                }
            } while (!entreEstOui(in) || !entreEstNon(in));
        }

        mainMenu();
    }

    private static boolean entreEstModiMenu (String  entre) {
        String[] confirmationDic = {"1", "2", "3", "4"};
        boolean check = Arrays.asList(confirmationDic).contains(entre);
        if (check) {
            return true;
        } else {
            afficherMenuDansMain(true);
            return false;
        }
    }

    private static String afficherEntreModification () {
        System.out.println("Entrez le numéro d'un fournisseur pour le modifier: ");
        System.out.println("* Vous avez besoin de rechercher le numéro par le nom? Entrez \"aide\"");
        String entre = scan.next();
        return entre;
    }

    private static void suprimerFourni() {
        if (cptLf == 0) {
            System.out.println(">> 0 enregistrement à supprimer <<\n");
            mainMenu();
        }

        String entre = afficherEntreSuppression();

        while (pasNumerique(entre) && !entre.equals("aide")) {
            entre = afficherEntreSuppression();
        }

        if (entre.equals("aide")) {
            rechercherParNom();
        } else if (!pasNumerique(entre)) {
            Fournisseur resultat = ListeFournisseur.findById(Integer.valueOf(entre), lf);
            afficherInfoIndividuelle(resultat);

            System.out.println("Confirmez de supprimer ce fournisseur? oui [o] ou non [n]");
            String in = scan.next();

            do {
                if (entreEstOui(in)) {
                    lf = ListeFournisseur.removeById(Integer.valueOf(entre), lf);
                    cptLf--;
                    System.out.println(">> Suppression effectuée avec succès <<");
                    mainMenu();
                } else if (entreEstNon(in)) {
                    System.out.println(">> Retouner au menu principal <<");
                    mainMenu();
                } else {
                    System.out.println("Veuillez saisir oui [o] ou non [n] pour confirmer la suppression: ");
                    in = scan.next();
                }
            } while (!entreEstOui(in) || !entreEstNon(in));
        }

        mainMenu();
    }

    private static String afficherEntreSuppression () {
        System.out.println("Entrez le numéro d'un fournisseur pour le supprimer: ");
        System.out.println("* Vous avez besoin de rechercher le numéro par le nom? Entrez \"aide\"");
        String entre = scan.next();
        return entre;
    }

    private static void afficherMenuDansMain(boolean reafficher) {
        System.out.println("\nTestLogistique - Bienvenue au système de gestion de fournisseurs\n");
        System.out.println("-- Autheur: Weijie JING Version: 1.0.0 --\n");
        System.out.println("1. Ajouter un nouveau fournisseur");
        System.out.println("2. Lister les fournisseurs");
        System.out.println("3. Rechercher un fournisseur par numéro");
        System.out.println("4. Rechercher un fournisseur par nom");
        System.out.println("5. Modifier des coordonnées d'un fournisseur");
        System.out.println("6. Supprimer d'un fournisseur par numéro");
        System.out.println("7. Quitter le système");
        System.out.println(!reafficher ? "\nVeuillez selectionner la commande:" : "\nVeuillez saisir un numéro correct du menu:");
    }

    private static void afficherMenuDansModi () {
        System.out.println("Entrez 1 pour modifier le nom");
        System.out.println("Entrez 2 pour modifier le numéro de téléphone");
        System.out.println("Entrez 3 pour modifier l'adresse de mail");
        System.out.println("Entrez 4 pour modifier l'état de commande");
    }

    private static void afficherInfoAenregistrer (String n, String np, String ma, boolean ec, boolean reafficher) {
        System.out.printf("\nNom de fournisseur: %s", n);
        System.out.printf("\nNuméro de téléphone:  %s", np);
        System.out.printf("\nAdresse de mail: %s", ma);
        System.out.printf("\nEtat de commande: %b", ec);
        System.out.println(!reafficher ? "\n\nConfirmez d'enregister le fournisseur? oui [o] ou non [n]" : "Veuillez saisir oui [o] ou non [n]");
    }

    private static void afficherInfoIndividuelle(Fournisseur f) {
        if (f == null) {
            System.out.println(">> L'enregistrement que vous recherchez n'existe pas <<");
        } else {
            System.out.printf("\n-----------");
            System.out.printf("\nNuméro de fournisseur: %d", f.id);
            System.out.printf("\nNom de fournisseur: %s", f.nom);
            System.out.printf("\nNuméro de téléphone: %s", f.noPhone);
            System.out.printf("\nAdresse de mail: %s", f.mailAddress);
            System.out.printf("\nEtat de commande: " + f.estCommande);
            System.out.printf("\n-----------\n");
        }
    }
}